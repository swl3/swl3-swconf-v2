package ru.swayfarer.swl3.swconf2.config.handlers;

import lombok.var;
import lombok.NonNull;
import ru.swayfarer.swl3.observable.property.ObservableProperty;
import ru.swayfarer.swl3.swconf2.config.Swconf2Config;
import ru.swayfarer.swl3.swconf2.mapper.annotations.IgnoreSwconf;

public class StandartConfigHandlers {

    public static void listenObservablePropertiesChanges(@NonNull Swconf2Config config)
    {
        var reflectionsUtils = config.getConfigHelper().getConfigInfo().getReflectionsUtils();
        var filterFilter = reflectionsUtils.fields().filters();

        var properties = reflectionsUtils.fields().stream(config)
            .not().filter(filterFilter.annotation().assignable(IgnoreSwconf.class))
            .filter(filterFilter.type().equal(ObservableProperty.class))
            .not().filter(filterFilter.mod().statics())
            .map((field) -> field.get(config))
            .toExList()
        ;
        
        for (var property : properties)
        {
            var value = (ObservableProperty<?>) property;
            
            value.eventPostChange.subscribe().by(() -> {
               config.config().getConfigInfo().getIsNeedsToSave().set(true);
            });
        }
    }
    
}
