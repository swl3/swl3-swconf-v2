package ru.swayfarer.swl3.swconf2.config;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.io.link.ResourceLink;
import ru.swayfarer.swl3.swconf2.config.Swconf2Config.ConfigInfo;
import ru.swayfarer.swl3.swconf2.config.thread.ConfigSaverThread;

@Data
@Accessors(chain = true)
public class ConfigHelper {
    
    @NonNull
    public Swconf2Config instance;
    
    public ConfigInfo configInfo = new ConfigInfo();
    
    public boolean isExists()
    {
        var rlink = configInfo.getRlink();
        return rlink == null || rlink.isExists();
    }
    
    public ConfigHelper createIfNotFound()
    {
        if (!isExists())
        {
            save();
        }
        
        return this;
    }
    
    public ConfigHelper enableAutoSave(long delay)
    {
        var thread = new ConfigSaverThread(instance);
        thread.setDelay(delay);
        thread.start();
        
        return this;
    }
    
    public ConfigHelper load(@NonNull ResourceLink rlLink)
    {
        return setLocation(rlLink).load();
    }
    
    public ConfigHelper load()
    {
        var rlink = configInfo.getRlink();
        
        if (rlink != null)
        {
            if (!isExists())
            {
                createIfNotFound();
            }
            else
            {
                configInfo.getSwconfIO().deserialize(instance, rlink);
            }
            
            configInfo.eventLoad.next(instance);
        }
        
        return this;
    }
    
    public ConfigHelper saveIfNeed()
    {
        if (configInfo.isNeedsToSave())
        {
            save();
        }
        
        return this;
    }
    
    public ConfigHelper save()
    {
        var rlink = configInfo.getRlink();
        
        if (rlink != null && rlink.isWritable())
        {
            configInfo.getSwconfIO().serialize(instance, rlink);
        }
        
        return this;
    }
    
    public ConfigHelper setLocation(@NonNull ResourceLink rlink)
    {
        configInfo.rlink = rlink;
        return this;
    }
}
