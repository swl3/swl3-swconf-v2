package ru.swayfarer.swl3.swconf2.config.properties;

import lombok.var;
import lombok.NonNull;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.swconf2.config.Swconf2Config;

public class ConfigPropertiesHelper {

    @NonNull
    public ConfigPropertiesReader propertiesReader = new ConfigPropertiesReader();
    
    public ConfigProperties getProperties(String prefix, @NonNull Swconf2Config config)
    {
        if (StringUtils.isBlank(prefix))
        {
            prefix = "";
        }
        
        var properties = propertiesReader.readProperties(prefix, config);
        
        var ret = new ConfigProperties(prefix, config, properties);
        
        return ret;
    }
}
