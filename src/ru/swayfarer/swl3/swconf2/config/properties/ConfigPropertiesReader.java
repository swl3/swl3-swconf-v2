package ru.swayfarer.swl3.swconf2.config.properties;

import lombok.var;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.swconf2.config.Swconf2Config;
import ru.swayfarer.swl3.swconf2.config.properties.ConfigProperties.PropertyAccessor;
import ru.swayfarer.swl3.swconf2.mapper.annotations.IgnoreSwconf;

@Data
@Accessors(chain = true)
public class ConfigPropertiesReader {
    
    public Map<String, PropertyAccessor> readProperties(String prefix, @NonNull Swconf2Config config)
    {
        var instance = config;
        var reflectionsUtils = config.getConfigHelper().getConfigInfo().getReflectionsUtils();
        var properties = new HashMap<String, PropertyAccessor>();
        readProperties(instance, reflectionsUtils, properties, prefix, new ArrayList<>());
        
        return Collections.synchronizedMap(properties);
    }
    
    public Map<String, PropertyAccessor> readProperties(Object instance, @NonNull ReflectionsUtils reflectionsUtils, @NonNull Map<String, PropertyAccessor> properties, @NonNull String prefix, @NonNull List<Object> visitedObjects)
    {
        if (instance == null)
            return properties;
        
        if (visitedObjects.stream().anyMatch((o) -> o == instance))
        {
            return properties;
        }
        
        visitedObjects.add(instance);

        var fieldFilter = reflectionsUtils.fields().filters();

        var fields = reflectionsUtils.fields().stream(instance)
                .not().filter(fieldFilter.mod().transients())
                .not().filter(fieldFilter.annotation().assignable(IgnoreSwconf.class))
                .not().filter(fieldFilter.mod().statics())
                .toExList()
        ;
        
        for (var field : fields)
        {
            var fieldName = field.getName();
            var fieldType = field.getType();
            
            var accessor = new PropertyAccessor(
                    () -> reflectionsUtils.fields().getValue(field, instance), 
                    (obj) -> reflectionsUtils.fields().setValue(field, instance, obj), 
                    fieldType
            );
            
            var name = StringUtils.isEmpty(prefix) ? fieldName : prefix + "." + fieldName;
            
            properties.put(name, accessor);
            
            if (!reflectionsUtils.types().isPrimitive(fieldType))
            {
                var fieldValue = accessor.getValue();
                readProperties(fieldValue, reflectionsUtils, properties, name, visitedObjects);
            }
        }
        
        return properties;
    }
}
