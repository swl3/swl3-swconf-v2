package ru.swayfarer.swl3.swconf2.config.properties;

import lombok.var;
import java.util.Map;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.swconf2.config.Swconf2Config;
import ru.swayfarer.swl3.swconf2.exception.PropertyNotFoundException;

@Data
@Accessors(chain = true)
@SuppressWarnings("unchecked")
public class ConfigProperties {
    
    @NonNull
    public String prefix;
    
    @NonNull
    public Swconf2Config config;
    
    @NonNull
    public Map<String, PropertyAccessor> properties;
    
    public ConfigProperties setProperty(@NonNull String propertyName, @NonNull Object value)
    {
        var accessor = properties.get(propertyName);
        ExceptionsUtils.IfNull(accessor, PropertyNotFoundException.class, "Can't find property with name", propertyName, "in", config);
        accessor.setValue(value);
        return this;
    }
    
    public <T> T getProperty(@NonNull String propertyName)
    {
        var accessor = properties.get(propertyName);
        ExceptionsUtils.IfNull(accessor, PropertyNotFoundException.class, "Can't find property with name", propertyName, "in", config);
        return accessor.getValue();
    }
    
    @Data
    @Accessors(chain = true)
    public static class PropertyAccessor {
        
        @NonNull
        public IFunction0<Object> getValueFun;
        
        @NonNull
        public IFunction1NoR<Object> setValueFun;
        
        @NonNull
        public Class<?> associatedType;
        
        public <T> T getValue()
        {
            return (T) getValueFun.apply();
        }
        
        public PropertyAccessor setValue(Object obj)
        {
            setValueFun.apply(obj);
            
            return this;
        }
    }
    
}
