package ru.swayfarer.swl3.swconf2.config.thread;

import lombok.var;
import java.util.concurrent.atomic.AtomicLong;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.swconf2.config.Swconf2Config;

@Data @EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class ConfigSaverThread extends Thread {

    @NonNull
    public Swconf2Config configInstance;
    
    public AtomicLong delay = new AtomicLong();
    
    @SneakyThrows
    @Override
    public void run()
    {
        var delay = this.delay.get();
        
        for (;;)
        {
            saveIfNeed();
            Thread.sleep(delay);
        }
    }
    
    public void saveIfNeed()
    {
        configInstance.config().saveIfNeed();
    }
    
    public ConfigSaverThread setDelay(long delay)
    {
        this.delay.set(delay);
        return this;
    }
    
    {
        setDaemon(true);
        Runtime.getRuntime().addShutdownHook(new Thread(this::saveIfNeed));
    }
}
