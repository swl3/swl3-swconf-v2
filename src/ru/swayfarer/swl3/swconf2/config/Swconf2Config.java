package ru.swayfarer.swl3.swconf2.config;

import lombok.var;
import java.util.concurrent.atomic.AtomicBoolean;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.ToString;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.io.link.ResourceLink;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.swconf2.config.handlers.StandartConfigHandlers;
import ru.swayfarer.swl3.swconf2.config.thread.ConfigSaverThread;
import ru.swayfarer.swl3.swconf2.helper.SwconfIO;
import ru.swayfarer.swl3.swconf2.mapper.annotations.IgnoreSwconf;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public abstract class Swconf2Config {

    @IgnoreSwconf
    public transient ConfigHelper configHelper = new ConfigHelper(this);

    public ConfigHelper config()
    {
        return configHelper;
    }
    
    @Data
    @Accessors(chain = true)
    public static class ConfigHelper {
        
        @ToString.Exclude
        @NonNull
        public Swconf2Config instance;
        
        public ConfigInfo configInfo = new ConfigInfo();
        
        public boolean isExists()
        {
            var rlink = configInfo.getRlink();
            return rlink == null || rlink.isExists();
        }
        
        public ConfigHelper createIfNotFound()
        {
            if (!isExists())
            {
                save();
            }
            
            return this;
        }
        
        public ConfigHelper enableAutoSave(long delay)
        {
            var thread = new ConfigSaverThread(instance);
            thread.setDelay(delay);
            thread.start();
            
            return this;
        }
        
        public ConfigHelper load(@NonNull ResourceLink rlLink)
        {
            return setLocation(rlLink).load();
        }
        
        public ConfigHelper load()
        {
            var rlink = configInfo.getRlink();
            
            if (rlink != null)
            {
                if (isExists())
                {
                    configInfo.getSwconfIO().deserialize(instance, rlink);
                }
                
                configInfo.eventLoad.next(instance);
            }
            
            return this;
        }
        
        public ConfigHelper saveIfNeed()
        {
            if (configInfo.isNeedsToSave())
            {
                save();
            }
            
            return this;
        }
        
        public ConfigHelper save()
        {
            var rlink = configInfo.getRlink();
            
            if (rlink != null && rlink.isWritable())
            {
                configInfo.getSwconfIO().serialize(instance, rlink);
            }
            
            return this;
        }
        
        public ConfigHelper setLocation(@NonNull ResourceLink rlink)
        {
            configInfo.rlink = rlink;
            return this;
        }
    }
    
    @Data
    @Accessors(chain = true)
    public static class ConfigInfo {
        
        public ResourceLink rlink;
        
        @NonNull
        public IObservable<Swconf2Config> eventLoad = Observables.createObservable();
        
        @NonNull
        public AtomicBoolean isNeedsToSave = new AtomicBoolean();
        
        @NonNull
        public SwconfIO swconfIO = new SwconfIO();
        
        @NonNull
        public ReflectionsUtils reflectionsUtils = new ReflectionsUtils();
        
        public boolean isNeedsToSave()
        {
            return isNeedsToSave.get();
        }
        
        public ConfigInfo setNeedsToSave(boolean isNeeds)
        {
            isNeedsToSave.set(isNeeds);
            return this;
        }
    }
    
    public void registerDefaultsLoadHandlers()
    {
        var eventLoad = config().getConfigInfo().eventLoad;
        eventLoad.subscribe().by(StandartConfigHandlers::listenObservablePropertiesChanges);
    }
    
    {
        registerDefaultsLoadHandlers();
    }
}
