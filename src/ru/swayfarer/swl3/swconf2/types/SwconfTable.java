package ru.swayfarer.swl3.swconf2.types;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.ToString;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.objects.EqualsUtils;
import ru.swayfarer.swl3.string.dynamic.DynamicString;

@Getter
@Setter
@Accessors(chain = true)
@SuppressWarnings("unchecked")
public class SwconfTable extends SwconfObject {

	public boolean hasKeys = true;
	public ExtendedList<TableEntry> entries = CollectionsSWL.list();
	
	public <T extends SwconfObject> ExtendedOptional<T> findFirst(@NonNull String key)
    {
        return entries.exStream()
                .filter((e) -> key.equals(e.key))
                .map(TableEntry::getValue)
                .map((e) -> (T) e)
                .findFirst()
        ;
    }
	
	public <T extends SwconfObject> ExtendedList<T> find(@NonNull String key)
	{
	    return entries.exStream()
	            .filter((e) -> key.equals(e.key))
	            .map(TableEntry::getValue)
	            .map((e) -> (T) e)
	            .toExList()
        ;
	}
	
	public <T extends SwconfTable> T put(SwconfObject value)
	{
		return put("", value);
	}
	
	public <T extends SwconfTable> T put(String key, SwconfObject value)
	{
		if (hasKeys)
			remove(key);

		entries.add(new TableEntry(key, value));
		return (T) this;
	}
	
	@Override
	public boolean isTable()
	{
		return true;
	}
	
	public int size()
	{
		return entries.size();
	}

	public ExtendedOptional<Byte> getByte(String key)
	{
		return getNumberValue(key, SwconfObject::isNum, Number::byteValue);
	}

	public ExtendedOptional<Short> getShort(String key)
	{
		return getNumberValue(key, SwconfObject::isNum, Number::shortValue);
	}

	public ExtendedOptional<Integer> getInt(String key)
	{
		return getNumberValue(key, SwconfObject::isNum, Number::intValue);
	}

	public ExtendedOptional<Long> getLong(String key)
	{
		return getNumberValue(key, SwconfObject::isNum, Number::longValue);
	}

	public ExtendedOptional<Float> getFloat(String key)
	{
		return getNumberValue(key, SwconfObject::isNum, Number::floatValue);
	}

	public ExtendedOptional<Double> getDouble(String key)
	{
		return getNumberValue(key, SwconfObject::isNum, Number::doubleValue);
	}

	public ExtendedOptional<Boolean> getBoolean(String key)
	{
		return getFilteredValue(key, SwconfObject::isBoolean, (e) -> e.asBoolean().getValue());
	}

	public ExtendedOptional<String> getString(String key)
	{
		return getFilteredValue(key, SwconfObject::isString, (e) -> e.asString().getValue());
	}

	public SwconfTable putByte(String key, byte value)
	{
		return put(key, new SwconfNum().setValue((double) value));
	}

	public SwconfTable putShort(String key, short value)
	{
		return put(key, new SwconfNum().setValue((double) value));
	}

	public SwconfTable putInt(String key, int value)
	{
		return put(key, new SwconfNum().setValue((double) value));
	}

	public SwconfTable putLong(String key, long value)
	{
		return put(key, new SwconfNum().setValue((double) value));
	}

	public SwconfTable putFloat(String key, float value)
	{
		return put(key, new SwconfNum().setValue((double) value));
	}

	public SwconfTable putDouble(String key, double value)
	{
		return put(key, new SwconfNum().setValue((double) value));
	}

	public SwconfTable putBoolean(String key, boolean value)
	{
		return put(key, new SwconfBoolean(value));
	}

	public SwconfTable putString(String key, String value)
	{
		return put(key, new SwconfString(value));
	}

	public SwconfTable putByte(byte value)
	{
		return put("", new SwconfNum().setValue((double) value));
	}

	public SwconfTable putShort(short value)
	{
		return put("", new SwconfNum().setValue((double) value));
	}

	public SwconfTable putInt(int value)
	{
		return put("", new SwconfNum().setValue((double) value));
	}

	public SwconfTable putLong(long value)
	{
		return put("", new SwconfNum().setValue((double) value));
	}

	public SwconfTable putFloat(float value)
	{
		return put("", new SwconfNum().setValue((double) value));
	}

	public SwconfTable putDouble(double value)
	{
		return put("", new SwconfNum().setValue((double) value));
	}

	public SwconfTable putBoolean(boolean value)
	{
		return put("", new SwconfBoolean(value));
	}

	public SwconfTable putString(String value)
	{
		return put("", new SwconfString(value));
	}

	public SwconfTable remove(String key)
	{
		entries.removeIf((e) -> EqualsUtils.objectEquals(key, e.getKey()));
		return this;
	}

	private <T> ExtendedOptional<T> getNumberValue(String key, IFunction1<SwconfObject, Boolean> filter, IFunction1<Number, T> valueFun)
	{
		return getFilteredValue(key, SwconfObject::isNum, (e) -> valueFun.apply(e.asNum().getValue()));
	}

	public <T> ExtendedOptional<T> getFilteredValue(String key, IFunction1<SwconfObject, Boolean> filter, IFunction1<SwconfObject, T> valueFun)
	{
		var first = findFirst(key);

		if (first.isEmpty())
			return ExtendedOptional.empty();

		var swconfObject = first.get();

		if (filter == null || Boolean.TRUE.equals(filter.apply(swconfObject)))
			return ExtendedOptional.of(valueFun.apply(swconfObject));

		return ExtendedOptional.empty();
	}

	@Override
	public String toString(int indent)
	{
		DynamicString ret = new DynamicString();
		
		ret.append("table: {");
		
		if (CollectionsSWL.isNullOrEmpty(entries))
		{
			ret.append("}");
		}
		else
		{
			ret.append("\n");
			
			for (var entry : entries)
			{
				ret.indent(indent + 1);
				
				if (hasKeys)
				{
					ret.append(entry.key);
					ret.append(" = ");
				}
				
				ret.append(entry.value.toString(indent + 1));
				ret.append("\n");
			}
			
			ret.indent(indent);
			ret.append("}");
		}
		
		return ret.toString();
	}
	
	@Data
	@ToString
	@AllArgsConstructor
	public static class TableEntry {
		public String key;
		public SwconfObject value;
	}
	
}
