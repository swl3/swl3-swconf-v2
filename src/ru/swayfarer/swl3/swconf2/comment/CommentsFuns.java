package ru.swayfarer.swl3.swconf2.comment;

import lombok.var;
import java.util.Properties;

import lombok.NonNull;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.io.link.RLUtils;
import ru.swayfarer.swl3.io.link.ResourceLink;
import ru.swayfarer.swl3.string.StringUtils;

public class CommentsFuns {
    
    public static IFunction1<String, String> ofProperties(@NonNull RLUtils rlutils, @NonNull String... props)
    {
        var properties = new Properties();
        
        ExtendedStream.of(props)
                .map(rlutils::createLink)
                .nonNull()
                .map(ResourceLink::in)
                .nonNull()
                .each(properties::load)
        ;
        
        return ofProperties(properties);
    }
    
    public static IFunction1<String, String> ofProperties(@NonNull Properties... props)
    {
        return (s) -> {
            
            if (StringUtils.isBlank(s))
                return null;
            
            for (var prop : props)
            {
                var ret = prop.get(s);
                
                if (ret != null)
                {
                    return ret.toString();
                }
            }
            
            return null;
        };
    }
}
