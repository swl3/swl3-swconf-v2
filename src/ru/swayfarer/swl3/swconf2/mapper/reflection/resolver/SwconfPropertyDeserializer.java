package ru.swayfarer.swl3.swconf2.mapper.reflection.resolver;

import lombok.var;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.swconf2.mapper.annotations.CommentedSwconf;
import ru.swayfarer.swl3.swconf2.mapper.standart.ReflectionMapper;
import ru.swayfarer.swl3.swconf2.types.SwconfObject;

public class SwconfPropertyDeserializer implements IReflectionPropertyDeserializer
{
    public IFunction1<String, String> fieldCommentFun = (s) -> s;

    @Override
    public void resolve(ReflectionMappingReadEvent event) throws Throwable
    {
        if (event.getResult() != null)
        {
            return;
        }

        var mappingInfo = event.getMappingInfo();
        var field = mappingInfo.getObjField();

        if (field != null)
        {
            var propertyName = ReflectionMapper.getCustomPropertyName(mappingInfo).orElse(field.getName());

            var classOfObj = mappingInfo.getObjType();
            SwconfObject swconfValue = event.getElements().get(propertyName);

            if (swconfValue != null)
            {
                CommentedSwconf commentAnnotation = field.getAnnotation(CommentedSwconf.class);

                if (commentAnnotation != null)
                {
                    var commentValue = commentAnnotation.value();

                    if (StringUtils.isBlank(commentValue))
                    {
                        commentValue = fieldCommentFun.apply(classOfObj.getName() + "." + field.getName());

                        if (!StringUtils.isBlank(commentValue))
                        {
                            swconfValue.setComment(commentValue);
                        }
                    }
                    else
                    {
                        swconfValue.setComment(commentValue);
                    }
                }

                var result = mappingInfo.getSerialization().deserialize(mappingInfo, swconfValue);

                event.setResult(result);
            }
        }
    }
}
