package ru.swayfarer.swl3.swconf2.mapper.reflection.resolver;

import lombok.var;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.chain.FunctionsChain1;
import ru.swayfarer.swl3.observable.event.AbstractEvent;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.converters.StringConverters;
import ru.swayfarer.swl3.string.dynamic.DynamicString;

@Getter
@Setter
@Accessors(chain = true)
public class SysEnvPropertyDeserializer implements IReflectionPropertyDeserializer
{
    public StringConverters stringConverters = StringConverters.getInstance();
    public FunctionsChain1<String, String> propertyResolverFuns = new FunctionsChain1<>();

    public SysEnvPropertyDeserializer()
    {
        propertyResolverFuns.add((IFunction1<String, String>) System::getenv);
        propertyResolverFuns.add((IFunction1<String, String>) System::getProperty);
    }

    @Override
    public void resolve(ReflectionMappingReadEvent event) throws Throwable
    {
        if (event.getResult() != null)
            return;

        var mappingInfo = event.getMappingInfo();

        var propertyNamePrefix = getPropertiesPrefix(mappingInfo);
        if (propertyNamePrefix == null)
            return;

        propertyNamePrefix = StringUtils.strip(propertyNamePrefix);

        var potentialNames = findPotentialPropertyNames(mappingInfo.getFullName(), propertyNamePrefix);

        String propertyStr = null;

        if (!CollectionsSWL.isNullOrEmpty(potentialNames))
        {
            for (var potentialName : potentialNames)
            {
                var resolved = propertyResolverFuns.apply(potentialName);

                if (resolved != null)
                {
                    propertyStr = resolved;
                    break;
                }
            }
        }

        if (propertyStr != null)
        {
            var converted = stringConverters.convert(mappingInfo.getObjType(), propertyStr);

            if (converted != null)
            {
                event.setResult(converted);
            }
        }
    }

    public static String getPropertiesPrefix(AbstractEvent event)
    {
        return event.getCustomAttributes().getValue("property-resolvers-prefix");
    }

    public static void setPropertiesPrefix(AbstractEvent event, String prefix)
    {
        event.getCustomAttributes().put("property-resolvers-prefix", prefix);
    }

    public ExtendedList<String> findPotentialPropertyNames(String name, String prefix)
    {
        var result = new ExtendedList<String>();
        var standPropertyName = prefix + "." + name;

        addWithAllCases(result, standPropertyName);
        addWithAllCases(result, standPropertyName.replace('.', '_'));

        var wordsSplitted = splitCamelCaseWords(standPropertyName);
        addWithAllCases(result, wordsSplitted);
        addWithAllCases(result, wordsSplitted.replace('.', '_'));

        return result;
    }

    private void addWithAllCases(ExtendedList<String> result, String standPropertyName)
    {
        result.add(standPropertyName);
        result.add(standPropertyName.toUpperCase());
        result.add(standPropertyName.toLowerCase());
    }

    public ExtendedList<String> asEnv(String name)
    {
        return null;
    }

    public String splitCamelCaseWords(String name)
    {
        var dynStr = new DynamicString();

        for (var ch : name.toCharArray())
        {
            if (Character.isUpperCase(ch))
            {
                if (!dynStr.isEmpty())
                    dynStr.append(".");

                dynStr.append(Character.toLowerCase(ch));
            }
            else
            {
                dynStr.append(ch);
            }
        }

        return dynStr.toString();
    }
}
