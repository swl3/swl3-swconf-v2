package ru.swayfarer.swl3.swconf2.mapper.reflection.resolver;

import lombok.var;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;

public interface IReflectionPropertyDeserializer extends IFunction1NoR<ReflectionMappingReadEvent>
{
    @Override
    default void applyNoRUnsafe(ReflectionMappingReadEvent swconfValueResolveEvent) throws Throwable
    {
        resolve(swconfValueResolveEvent);
    }

    public void resolve(ReflectionMappingReadEvent event) throws Throwable;
}
