package ru.swayfarer.swl3.swconf2.mapper.reflection.resolver;

import lombok.var;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.observable.event.AbstractEvent;
import ru.swayfarer.swl3.swconf2.mapper.MappingInfo;
import ru.swayfarer.swl3.swconf2.types.SwconfObject;

import java.util.Map;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class ReflectionMappingReadEvent extends AbstractEvent
{
    @NonNull
    public MappingInfo mappingInfo;

    @NonNull
    public Map<String, SwconfObject> elements;

    public Object result;
}
