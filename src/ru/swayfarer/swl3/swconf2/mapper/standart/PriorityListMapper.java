package ru.swayfarer.swl3.swconf2.mapper.standart;

import lombok.var;
import ru.swayfarer.swl3.collections.list.PriorityList;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.swconf2.mapper.ISwconfMapper;
import ru.swayfarer.swl3.swconf2.mapper.MappingInfo;
import ru.swayfarer.swl3.swconf2.mapper.SwconfMappingException;
import ru.swayfarer.swl3.swconf2.types.SwconfObject;
import ru.swayfarer.swl3.swconf2.types.SwconfTable;

@SuppressWarnings( {"rawtypes", "unchecked"} )
public class PriorityListMapper implements ISwconfMapper<SwconfTable, PriorityList>{

    @Override
    public boolean isAccepts(MappingInfo mappingInfo)
    {
        var equals = PriorityList.class.equals(mappingInfo.getObjType());
        return equals;
    }
    
    @Override
    public void read(SwconfTable swconf, MappingInfo mappingInfo)
    {
        var list = (PriorityList) mappingInfo.getObj(); 
        
        if (list == null)
            list = new PriorityList<>();
        
        var objField = mappingInfo.getObjField();
        
        ExceptionsUtils.If(mappingInfo.hasNoGenerics(), SwconfMappingException.class, "No generics found in field", objField, "! Select list element type in <> for map it!");
        
        var generics = mappingInfo.generics;
        
        ExceptionsUtils.If(generics.size() > 1, SwconfMappingException.class, "To many generics in field", objField, "! Field must has a single generic with list element type!");
        
        for (var entry : swconf.entries)
        {
            SwconfObject value = entry.value;
            
            if (value != null && value.isTable())
            {
                var table = value.asTable();
                var priorityNode = table.findFirst("priority").orNull();
                var valueNode = table.findFirst("value").orNull();
                
                var typeGeneric = generics.first();
                
                Integer entryPriority = null;
                
                if (priorityNode != null)
                {
                    var priorityMappingInfo = MappingInfo.builder()
                            .objType(Integer.class)
                            .serialization(mappingInfo.getSerialization())
                    .build();

                    priorityMappingInfo.setCustomAttributes(mappingInfo.getCustomAttributes());
                    
                    mappingInfo.getSerialization().deserialize(priorityMappingInfo, priorityNode);
                    entryPriority = (Integer) priorityMappingInfo.getObj();
                }
                
                if (valueNode != null)
                {
                    var valueMappingInfo = MappingInfo.builder()
                            .objType(typeGeneric.loadClass())
                            .generics(typeGeneric.getChilds())
                            .serialization(mappingInfo.getSerialization())
                    .build();

                    valueMappingInfo.setCustomAttributes(mappingInfo.getCustomAttributes());

                    mappingInfo.getSerialization().deserialize(valueMappingInfo, valueNode);
                    var entryValue = valueMappingInfo.getObj();
                    
                    if (entryValue != null)
                    {
                        if (entryPriority == null)
                            entryPriority = 0;
                        
                        list.add(entryValue, entryPriority);
                    }
                }
            }
        }
        
        mappingInfo.setObj(list);
    }

    @Override
    public SwconfTable write(MappingInfo mappingInfo)
    {
        var list = (PriorityList) mappingInfo.getObj();
        
        if (list == null)
            return null;

        SwconfTable table = mappingInfo.getOrCreateInitialRoot(
                (initial) -> initial != null && initial.isTable() && !initial.asTable().hasKeys,
                () -> new SwconfTable().setHasKeys(false)
        );

        for (var element : list)
        {
            var elementMappingInfo = MappingInfo.builder()
                    .obj(element)
                    .objType(element.getClass())
                    .serialization(mappingInfo.getSerialization())
            .build();

            elementMappingInfo.setCustomAttributes(mappingInfo.getCustomAttributes());
            
            var elementSwconf = mappingInfo.getSerialization().serialize(elementMappingInfo);
            
            if (elementSwconf != null)
            {
                table.put(elementSwconf);
            }
        }
        
        return table;
    }
}

