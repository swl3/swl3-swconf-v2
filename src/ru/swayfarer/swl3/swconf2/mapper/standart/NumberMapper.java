package ru.swayfarer.swl3.swconf2.mapper.standart;

import lombok.var;
import java.lang.reflect.Method;

import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.swconf2.mapper.ISwconfMapper;
import ru.swayfarer.swl3.swconf2.mapper.MappingInfo;
import ru.swayfarer.swl3.swconf2.types.SwconfNum;
import ru.swayfarer.swl3.swconf2.types.SwconfObject;
import ru.swayfarer.swl3.swconf2.types.SwconfString;

public class NumberMapper implements ISwconfMapper<SwconfObject, Object> {

	public Method valueMethod;
	
	public ExtendedList<Class<?>> acceptableTypes = CollectionsSWL.list();
	
	public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
	public ReflectionsUtils reflectionsUtils = ReflectionsUtils.getInstance();
	
	public NumberMapper(String valueMethodName, Class<?>... acceptableClasses)
	{
		acceptableTypes = CollectionsSWL.list(acceptableClasses);
		var methodFilters = reflectionsUtils.methods().filters();

		valueMethod =  reflectionsUtils.methods().stream(Double.class)
			.filter(methodFilters.name().equal(valueMethodName + "Value"))
		    .findFirst().orNull()
	    ;
	}
	
	@Override
	public boolean isAccepts(MappingInfo mappingInfo)
	{
	    var ret = acceptableTypes.contains(mappingInfo.getObjType());
		return ret;
	}

	@Override
	public void read(SwconfObject swconf, MappingInfo mappingInfo)
	{
	    ExceptionsUtils.IfNot(
	            swconf.isNum() || swconf.isString(), 
	            IllegalArgumentException.class, 
	            "Swconf '", swconf.getName(), "' must be a number or string!"
        );
	        
		var value = Double.valueOf(swconf.getValue().toString());
		
		exceptionsHandler.safe(() -> {
			var obj = valueMethod.invoke(value);
			
			mappingInfo.setObj(obj);
			
		}, "Error while creating number value");
	}

	@Override
	public SwconfNum write(MappingInfo mappingInfo)
	{
		SwconfNum num = mappingInfo.getOrCreateInitialRoot(
				(initial) -> initial != null && initial.isNum(),
				() -> new SwconfNum()
		);

		num.setValue(Double.valueOf(mappingInfo.getObj().toString()));

		return num;
	}

}
