package ru.swayfarer.swl3.swconf2.mapper.standart;

import lombok.var;
import lombok.Builder;
import lombok.NonNull;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.swconf2.mapper.ISwconfMapper;
import ru.swayfarer.swl3.swconf2.mapper.MappingInfo;
import ru.swayfarer.swl3.swconf2.mapper.SwconfMappingException;
import ru.swayfarer.swl3.swconf2.types.SwconfObject;

@Builder
public class ContstructorMapper <Result_Type> implements ISwconfMapper<SwconfObject, Result_Type> {

    @Builder.Default
    public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
    
    @Builder.Default
    public ReflectionsUtils reflectionsUtils = ReflectionsUtils.getInstance();
    
	@Builder.Default
	public ExtendedList<Class<?>> acceptableClasses = CollectionsSWL.list();
	
	@NonNull
	public Class<?> classOfObj;
	
	@NonNull
	public Class<? extends SwconfObject> classOfSwconf;

	@Override
	public boolean isAccepts(MappingInfo mappingInfo)
	{
		return acceptableClasses.stream()
				.anyMatch((type) -> type.isAssignableFrom(mappingInfo.getObjType()));
	}

	@Override
	public void read(SwconfObject swconf, MappingInfo mappingInfo)
	{
		ExceptionsUtils.IfNot(swconf.hasValue(), SwconfMappingException.class, "Can't read value from", swconf);
		var value = reflectionsUtils.constructors().newInstance(classOfObj, swconf.getValue()).orHandle(exceptionsHandler, "Error while creating new instance of", classOfObj);

		mappingInfo.setObj(value);
	}

	@Override
	public SwconfObject write(MappingInfo mappingInfo)
	{
		SwconfObject swconf = reflectionsUtils.constructors().newInstance(classOfSwconf).orHandle(exceptionsHandler, "Error while creating new instance of", classOfSwconf);
		ExceptionsUtils.IfNot(swconf.hasValue(), SwconfMappingException.class, "Can't write value to", swconf);
		swconf.setRawValue(mappingInfo.getObj());
		return swconf;
	}
}
