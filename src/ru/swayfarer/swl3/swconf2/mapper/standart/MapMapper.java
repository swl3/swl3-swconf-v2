package ru.swayfarer.swl3.swconf2.mapper.standart;

import lombok.var;
import java.util.HashMap;
import java.util.Map;

import lombok.NonNull;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.reflection.generic.GenericObject;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.converters.StringConverters;
import ru.swayfarer.swl3.swconf2.mapper.ISwconfMapper;
import ru.swayfarer.swl3.swconf2.mapper.MappingInfo;
import ru.swayfarer.swl3.swconf2.mapper.SwconfMappingException;
import ru.swayfarer.swl3.swconf2.types.SwconfTable;

@SuppressWarnings( {"rawtypes", "unchecked"} )
public class MapMapper implements ISwconfMapper<SwconfTable, Map> {

//	public StringParsers stringParsers = new StringParsers().registerDefaultProviders();
    
    public StringConverters stringConverters = new StringConverters().defaultTypes();
	
	public ExtendedList<IFunction1<MappingInfo, Map<?,?>>> registeredFactories = CollectionsSWL.list();
	
	public <T extends MapMapper> T registerCreationFun(IFunction1<MappingInfo, Map> fun)
	{
		this.registeredFactories.addExclusive(ReflectionsUtils.cast(fun));
		return (T) this;
	}
	
	public Map createMap(MappingInfo mappingInfo)
	{
		for (var factoryFun : registeredFactories)
		{
			if (factoryFun != null)
			{
				var ret = factoryFun.apply(mappingInfo);
				
				if (ret != null)
				{
					return ret;
				}
			}
		}
		
		return null;
	}
	
	@Override
	public boolean isAccepts(MappingInfo mappingInfo)
	{
		return Map.class.isAssignableFrom(mappingInfo.getObjType());
	}

	@Override
	public void read(SwconfTable swconf, MappingInfo mappingInfo)
	{
		var generics = mappingInfo.getGenerics();
		
		ExceptionsUtils.If(CollectionsSWL.isNullOrEmpty(generics) || generics.size() != 2, SwconfMappingException.class, "Invalid generic type of field", mappingInfo.getObjField(), "! It must be a 2 generics(as <A, B>)!");
		ExceptionsUtils.If(!swconf.hasKeys, SwconfMappingException.class, "Can't read map from non-keyed table", swconf.toString(0));

		var keyGeneric = generics.get(0);
		var typeGeneric = generics.get(1);
			
		Class<?> classOfKey = keyGeneric.loadClass();
		
		ExceptionsUtils.IfNull(classOfKey, SwconfMappingException.class, "Can't get key class for field", mappingInfo.getObjField());
		
		Map map = (Map) mappingInfo.getObj();
		
		if (map == null)
		{
			map = createMap(mappingInfo);
		}
		
		if (typeGeneric != null)
		{
			for (var entry : swconf.entries)
			{
				var key = entry.key;
				var value = entry.value;
				var childGenerics = typeGeneric.getChilds();
				
				if (!StringUtils.isBlank(key) && value != null)
				{
					var entryMappingInfo = MappingInfo.builder()
							.objType(typeGeneric.loadClass())
							.generics(childGenerics)
							.serialization(mappingInfo.getSerialization())
					.build();

					entryMappingInfo.setCustomAttributes(mappingInfo.getCustomAttributes());
					entryMappingInfo.appendFullName(mappingInfo, key);

					var readedValue = mappingInfo.getSerialization().deserialize(entryMappingInfo, value);
					
					Object keyValue = key;
					
					if (classOfKey != String.class)
					{
					    var converted = stringConverters.convert(classOfKey, key);
					    
						if (converted != null)
						{
							keyValue = converted;
						}
						else
						{
							throw new SwconfMappingException("Can't parse key '" + key + "' to type " + classOfKey.getName() + "!. Please, register specified mapper in StringParsers class or use another key type!");
						}
					}
					
					map.put(keyValue, readedValue);
				}
			}
		}
		
		mappingInfo.setObj(map);
	}

	@Override
	public SwconfTable write(MappingInfo mappingInfo)
	{
		Map<?, ?> map = (Map) mappingInfo.getObj();
		
		if (map == null)
			return null;

		SwconfTable table = mappingInfo.getOrCreateInitialRoot(
				(initial) -> initial != null && initial.isTable() && initial.asTable().hasKeys,
				() -> new SwconfTable()
		);

		for (var entry : map.entrySet())
		{
			var key = entry.getKey().toString();
			var value = entry.getValue();

			Class<?> valueType = null;

			ExceptionsUtils.IfNot(mappingInfo.getGenerics().size() == 2, IllegalStateException.class, "Type must have 2 generics (as Map<Key, Value>), but found", mappingInfo.getGenerics().size());

			if (value == null)
				valueType = mappingInfo.getGenerics().get(1).loadClass();
			else
				valueType = value.getClass();

			var valueMappingInfo = MappingInfo.builder()
					.obj(value)
					.objType(valueType)
					.generics(mappingInfo.getGenerics().get(1).getChilds())
					.serialization(mappingInfo.getSerialization())
			.build();

			valueMappingInfo.setCustomAttributes(mappingInfo.getCustomAttributes());
			valueMappingInfo.appendFullName(mappingInfo, key);

			var swconfValue = mappingInfo.getSerialization().serialize(valueMappingInfo);
			
			if (swconfValue != null)
			{
				table.put(key, swconfValue);
			}
		}
		
		return table;
	}

	public <T extends MapMapper> T registerDefaultCreationFuns(@NonNull ReflectionsUtils reflectionsUtils)
	{
	    registerCreationFun((mappingInfo) -> {
	        var type = mappingInfo.getObjType();
	        
	        if (Map.class.isAssignableFrom(type))
	            return (Map) reflectionsUtils.constructors().newInstance(type).returnValue();
	        
	        return null;
	    });
	    
		registerCreationFun((mappingInfo) -> {
			var type = mappingInfo.getObjType();
			
			if (Map.class.isAssignableFrom(type))
				return new HashMap<>();
			
			return null;
		});
		return (T) this;
	}
}
