package ru.swayfarer.swl3.swconf2.mapper.standart;

import lombok.var;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.swconf2.mapper.ISwconfMapper;
import ru.swayfarer.swl3.swconf2.mapper.MappingInfo;
import ru.swayfarer.swl3.swconf2.types.SwconfString;

public class EnumMapper implements ISwconfMapper<SwconfString, Object> {

    public ReflectionsUtils reflectionsUtils = ReflectionsUtils.getInstance();
    public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
    
	@Override
	public boolean isAccepts(MappingInfo mappingInfo)
	{
		Class<?> objType = mappingInfo.getObjType();
		return objType.isEnum();
	}

	@Override
	public void read(SwconfString swconf, MappingInfo mappingInfo)
	{
		var obj = mappingInfo.getObj();
		
		if (obj != null)
			return;

		var classOfEnum = mappingInfo.getObjType();
		
		obj = reflectionsUtils.methods().invoke(classOfEnum, "valueOf", swconf.getValue()).orHandle(exceptionsHandler, "Error while mapping", swconf.getValue(), "to", classOfEnum);
		
		mappingInfo.setObj(obj);
	}

	@Override
	public SwconfString write(MappingInfo mappingInfo)
	{
		var obj = mappingInfo.getObj();
		
		if (obj == null)
			return null;

		SwconfString str = mappingInfo.getOrCreateInitialRoot(
				(initial) -> initial != null && initial.isString(),
				() -> new SwconfString()
		);

		str.setValue(((Enum) obj).name());

		return str;
	}
}
