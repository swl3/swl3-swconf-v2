package ru.swayfarer.swl3.swconf2.mapper.standart;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.swconf2.mapper.ISwconfMapper;
import ru.swayfarer.swl3.swconf2.mapper.MappingInfo;
import ru.swayfarer.swl3.swconf2.types.SwconfString;
import ru.swayfarer.swl3.swconf2.types.SwconfTable;

@Getter
@Setter
@Accessors(chain = true)
public class StringValueOfMapper implements ISwconfMapper<SwconfString, Object>
{
    public String methodName = "valueOf";
    public ReflectionsUtils reflectionsUtils = ReflectionsUtils.getInstance();
    public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();

    public ExtendedList<Class<?>> acceptableTypes = new ExtendedList<>();

    public StringValueOfMapper(Class<?>... acceptableTypes)
    {
        this(CollectionsSWL.list(acceptableTypes));
    }

    public StringValueOfMapper(ExtendedList<Class<?>> acceptableTypes)
    {
        this.acceptableTypes = acceptableTypes;
    }

    @Override
    public boolean isAccepts(MappingInfo mappingInfo)
    {
        Class<?> objType = mappingInfo.getObjType();
        return acceptableTypes.contains(objType);
    }

    @Override
    public void read(SwconfString swconf, MappingInfo mappingInfo)
    {
        var obj = mappingInfo.getObj();

        if (obj != null)
            return;

        var classOfObj = mappingInfo.getObjType();

        obj = reflectionsUtils.methods().invoke(classOfObj, methodName, swconf.getValue()).orHandle(exceptionsHandler, "Error while mapping", swconf.getValue(), "to", classOfObj);

        mappingInfo.setObj(obj);
    }

    @Override
    public SwconfString write(MappingInfo mappingInfo)
    {
        var obj = mappingInfo.getObj();

        if (obj == null)
            return null;

        SwconfString str = mappingInfo.getOrCreateInitialRoot(
                (initial) -> initial != null && initial.isString(),
                () -> new SwconfString()
        );

        str.setValue(String.valueOf(obj));

        return str;
    }
}
