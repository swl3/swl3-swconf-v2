package ru.swayfarer.swl3.swconf2.mapper.standart;

import lombok.var;
import ru.swayfarer.swl3.string.ExpressionsList;
import ru.swayfarer.swl3.swconf2.mapper.ISwconfMapper;
import ru.swayfarer.swl3.swconf2.mapper.MappingInfo;
import ru.swayfarer.swl3.swconf2.types.SwconfObject;
import ru.swayfarer.swl3.swconf2.types.SwconfTable;

public class ExpressionsListMapper implements ISwconfMapper<SwconfTable, ExpressionsList>{

    @Override
    public boolean isAccepts(MappingInfo mappingInfo)
    {
        return ExpressionsList.class.equals(mappingInfo.getObjType());
    }

    @Override
    public void read(SwconfTable swconf, MappingInfo mappingInfo)
    {
        var list = (ExpressionsList) mappingInfo.getObj(); 
        
        if (list == null)
            list = new ExpressionsList();
        
        for (var entry : swconf.entries)
        {
            SwconfObject value = entry.value;
            
            if (value != null)
            {
                var entryMappingInfo = MappingInfo.builder()
                        .objType(String.class)
                        .serialization(mappingInfo.getSerialization())
                .build();

                entryMappingInfo.setCustomAttributes(mappingInfo.getCustomAttributes());
                
                mappingInfo.getSerialization().deserialize(entryMappingInfo, value);
                
                list.add((String) entryMappingInfo.getObj());
            }
        }
        
        mappingInfo.setObj(list);
    }

    @Override
    public SwconfTable write(MappingInfo mappingInfo)
    {
        var list = (ExpressionsList) mappingInfo.getObj();
        
        if (list == null)
            return null;

        SwconfTable table = mappingInfo.getOrCreateInitialRoot(
                (initial) -> initial != null && initial.isTable() && !initial.asTable().hasKeys,
                () -> new SwconfTable().setHasKeys(false)
        );
        
        for (var element : list)
        {
            var elementMappingInfo = MappingInfo.builder()
                    .obj(element)
                    .objType(element.getClass())
                    .serialization(mappingInfo.getSerialization())
            .build();

            elementMappingInfo.setCustomAttributes(mappingInfo.getCustomAttributes());
            
            var elementSwconf = mappingInfo.getSerialization().serialize(elementMappingInfo);
            
            if (elementSwconf != null)
            {
                table.put(elementSwconf);
            }
        }
        
        return table;
    }
}
