package ru.swayfarer.swl3.swconf2.mapper.standart;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Map;
import java.util.WeakHashMap;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.observable.event.AbstractCancelableEvent;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.swconf2.mapper.ISwconfMapper;
import ru.swayfarer.swl3.swconf2.mapper.MappingInfo;
import ru.swayfarer.swl3.swconf2.mapper.SwconfMappingException;
import ru.swayfarer.swl3.swconf2.mapper.SwconfSerialization;
import ru.swayfarer.swl3.swconf2.mapper.annotations.CommentedSwconf;
import ru.swayfarer.swl3.swconf2.mapper.annotations.EnableEnvs;
import ru.swayfarer.swl3.swconf2.mapper.annotations.IgnoreSwconf;
import ru.swayfarer.swl3.swconf2.mapper.annotations.IncludeSwconf;
import ru.swayfarer.swl3.swconf2.mapper.annotations.SwconfPropertyName;
import ru.swayfarer.swl3.swconf2.mapper.reflection.resolver.ReflectionMappingReadEvent;
import ru.swayfarer.swl3.swconf2.mapper.reflection.resolver.SwconfPropertyDeserializer;
import ru.swayfarer.swl3.swconf2.mapper.reflection.resolver.SysEnvPropertyDeserializer;
import ru.swayfarer.swl3.swconf2.types.SwconfObject;
import ru.swayfarer.swl3.swconf2.types.SwconfTable;
import ru.swayfarer.swl3.thread.ThreadsUtils;

@SuppressWarnings("unchecked")
@Getter
@Setter
@Accessors(chain = true)
public class ReflectionMapper implements ISwconfMapper<SwconfObject, Object> {

    public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
    public ReflectionsUtils reflectionsUtils = ReflectionsUtils.getInstance();
	public IObservable<MappingEvent> eventMapping = Observables.createObservable();
	public IObservable<ReflectionMappingReadEvent> eventDeserialize = Observables.createObservable();

	public ReflectionMapper()
	{
		eventDeserialize.subscribe().by(new SwconfPropertyDeserializer(), 1);
		eventDeserialize.subscribe().by(new SysEnvPropertyDeserializer(), 0);
	}

	@Override
	public boolean isAccepts(MappingInfo mappingInfo)
	{
		return mappingInfo.getObjType() instanceof Object;
	}

	@Override
	public void read(SwconfObject swconf, MappingInfo mappingInfo)
	{
		ExceptionsUtils.IfNot(swconf.isTable(), SwconfMappingException.class, "Swconf", swconf.toString(0), " must be a table!");
		SwconfTable table = swconf.asTable();
		ExceptionsUtils.IfNot(table.hasKeys, SwconfMappingException.class, "Swconf table", swconf.toString(0), " must has a keys!");
		
		Map<String, SwconfObject> elements = table.entries.exStream()
				.toMap((e) -> e.key, (e) -> e.value);
		
		Object obj = mappingInfo.getObj();
		
		Class<?> classOfObj = mappingInfo.getObjType();
		
		if (obj == null)
		{
			obj = reflectionsUtils.constructors()
			        .newInstance(mappingInfo.objType)
			        .orHandle(exceptionsHandler, "Error while creating object", mappingInfo.objType)
	        ;
			
			ExceptionsUtils.IfNull(obj, SwconfMappingException.class, "Can't create a new instance of class", classOfObj, " maybe default(no args) constructor does not exist?");
		}
		
		final Object instace = obj;

		var fieldsFilter = reflectionsUtils.fields().filters();

		reflectionsUtils.fields().stream(classOfObj)
		.not().filter(fieldsFilter.mod().transients())
		.each((field) -> {
			exceptionsHandler.safe(() -> {
				var fieldMapping = MappingInfo.ofField(field, classOfObj, instace, reflectionsUtils)
						.serialization(mappingInfo.getSerialization())
						.obj(null)
				.build();

				fieldMapping.setCustomAttributes(mappingInfo.getCustomAttributes());

				var mappingEvent = MappingEvent.builder()
						.mappingInfo(fieldMapping)
				.build();

				eventMapping.next(mappingEvent);

				var propertyName = getCustomPropertyName(fieldMapping).orElse(field.getName());

				fieldMapping.appendFullName(mappingInfo, propertyName);

				if (!mappingEvent.isCanceled())
				{
					var event = ReflectionMappingReadEvent.builder()
							.elements(elements)
							.mappingInfo(fieldMapping)
							.build()
					;



					eventDeserialize.next(event);

					var result = event.getResult();

					if (result != null)
						field.set(instace, result);
				}
			}, "Error while setting field", field, "value");
		});
		
		mappingInfo.setObj(obj);
	}

	public String resolveComment(MappingInfo mappingInfo, SwconfObject object)
	{
		var oldComment = object.getComment();

		if (!StringUtils.isBlank(oldComment))
			return oldComment;

		return mappingInfo.getSerialization().commentFuns.apply(mappingInfo.getFullName());
	}

	@Override
	public SwconfTable write(MappingInfo mappingInfo)
	{
		Object obj = mappingInfo.getObj();
		
		if (obj == null)
			return null;

		SwconfTable table = mappingInfo.getOrCreateInitialRoot(
				(initial) -> initial != null && initial.isTable() && initial.asTable().hasKeys,
				SwconfTable::new
		);

		Class<?> classOfObj = obj.getClass();

		var fieldsFilter = reflectionsUtils.fields().filters();

		reflectionsUtils.fields().stream(classOfObj)
        .not().filter(fieldsFilter.mod().transients())
		.each((field) -> {
			exceptionsHandler.safe(() -> {
				
				var fieldValue = field.get(obj);
				
				if (fieldValue != null)
				{
					var fieldValueMapping = MappingInfo.ofField(field, classOfObj, obj, reflectionsUtils)
							.serialization(mappingInfo.getSerialization())
					.build();

					fieldValueMapping.setCustomAttributes(mappingInfo.getCustomAttributes().copy());

					var mappingEvent = MappingEvent.builder()
							.mappingInfo(fieldValueMapping)
					.build();

					eventMapping.next(mappingEvent);

					var propertyName = getCustomPropertyName(fieldValueMapping).orElse(field.getName());
					fieldValueMapping.appendFullName(mappingInfo, propertyName);

					if (!mappingEvent.isCanceled())
					{
						SwconfObject swconfValue = mappingInfo.getSerialization().serialize(fieldValueMapping);

						reflectionsUtils.annotations().findProperty()
								.name("value")
								.type(String.class)
								.marker(CommentedSwconf.class)
								.in(field)
								.optional()
								.ifPresent((value) -> {
									var comment = resolveComment(fieldValueMapping, swconfValue);
									swconfValue.setComment(comment);
								})
						;
						
						table.put(propertyName, swconfValue);
					}
				}
				
			}, "Error while mapping field", field, "value");
		});
		
		return table;
	}
	
	public <T extends ReflectionMapper> T registerDefaultEvents()
	{
		var cachedPerFieldIncludeClasses = ThreadsUtils.threadLocal(() -> new ExtendedMap<Class<?>, Boolean>(new WeakHashMap<>()));

		eventMapping.subscribe().by((e) -> {
			var field = e.getMappingInfo().getObjField();
			if (field != null)
			{
				var customPropertyName = reflectionsUtils.annotations().findProperty()
						.name("value")
						.type(String.class)
						.marker(SwconfPropertyName.class)
						.in(field)
						.get()
				;


				if (!StringUtils.isBlank(customPropertyName))
				{
					setCustomPropertyName(customPropertyName, e.getMappingInfo());
				}
			}
		});

		eventMapping.subscribe().by((e) -> {
			var objType = e.getMappingInfo().getOwnerClass();

			var includeOnly = cachedPerFieldIncludeClasses.apply().getOrCreate(objType, () -> {
				var filters = reflectionsUtils.fields().filters();

				var isClassAnnotated = reflectionsUtils.annotations().findAnnotation()
						.ofType(IncludeSwconf.class)
						.in(objType)
						.get() != null
				;

				if (isClassAnnotated)
					return true;

				return reflectionsUtils.fields().stream(objType)
						.filter(filters.annotation().assignable(IncludeSwconf.class))
						.findAny().isPresent()
				;
			});

			if (Boolean.TRUE.equals(includeOnly))
			{
				var field = e.getMappingInfo().getObjField();
				var includeAnnotation = reflectionsUtils.annotations().findAnnotation()
						.ofType(IncludeSwconf.class)
						.in(field)
						.get()
				;

				if (includeAnnotation == null)
					e.setCanceled(true);
			}
		});

		eventMapping.subscribe().by((e) -> {
			var field = e.getMappingInfo().getObjField();
			
			var annotation = reflectionsUtils.annotations().findAnnotation()
			    .ofType(IgnoreSwconf.class)
			    .in(field)
			    .get()
		    ;
			 
			if (annotation != null)
			{
				e.setCanceled(true);
			}
		});

		eventMapping.subscribe().by((e) -> {
			var instanceType = e.getMappingInfo().getOwnerInstance().getClass();

			var propertiesPrefix = reflectionsUtils.annotations().findProperty()
					.type(String.class)
					.name("prefix")
					.marker(EnableEnvs.class)
					.in(instanceType)
					.get()
			;

			if (propertiesPrefix != null)
			{
				SysEnvPropertyDeserializer.setPropertiesPrefix(e.getMappingInfo(), propertiesPrefix);
			}
		});
		
		return (T) this;
	}

	public static void setCustomPropertyName(String name, MappingInfo mappingInfo)
	{
		mappingInfo.getCustomAttributes().put("swl3-property-custom-name", name);
	}

	public static ExtendedOptional<String> getCustomPropertyName(MappingInfo mappingInfo)
	{
		return ExtendedOptional.of(mappingInfo.getCustomAttributes().getValue("swl3-property-custom-name"));
	}
	
	@EqualsAndHashCode(callSuper = false)
	@Data
	@Builder
	public static class MappingEvent extends AbstractCancelableEvent {
		public MappingInfo mappingInfo;
	}

}
