package ru.swayfarer.swl3.swconf2.mapper.standart;

import lombok.var;
import java.util.ArrayList;
import java.util.List;

import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.reflection.generic.GenericObject;
import ru.swayfarer.swl3.swconf2.mapper.ISwconfMapper;
import ru.swayfarer.swl3.swconf2.mapper.MappingInfo;
import ru.swayfarer.swl3.swconf2.mapper.SwconfMappingException;
import ru.swayfarer.swl3.swconf2.types.SwconfObject;
import ru.swayfarer.swl3.swconf2.types.SwconfTable;

@SuppressWarnings( {"rawtypes", "unchecked"} )
public class ListMapper implements ISwconfMapper<SwconfTable, List>{

	public ExtendedList<IFunction1<MappingInfo, List<Object>>> registeredFactories = CollectionsSWL.list();
	
	@Override
	public boolean isAccepts(MappingInfo mappingInfo)
	{
		return List.class.isAssignableFrom(mappingInfo.getObjType());
	}
	
	public <T extends ListMapper> T registerCreationFun(IFunction1<MappingInfo, List<?>> fun)
	{
		this.registeredFactories.addExclusive(ReflectionsUtils.cast(fun));
		return (T) this;
	}

	public List createList(MappingInfo mappingInfo)
	{
		for (var factoryFun : registeredFactories)
		{
			if (factoryFun != null)
			{
				var ret = factoryFun.apply(mappingInfo);
				
				if (ret != null)
				{
					return ret;
				}
			}
		}
		
		return null;
	}
	
	@Override
	public void read(SwconfTable swconf, MappingInfo mappingInfo)
	{
		List list = (List) mappingInfo.getObj(); 
		
		if (list == null)
			list = createList(mappingInfo);
		
		var objField = mappingInfo.getObjField();
		
		ExceptionsUtils.If(mappingInfo.hasNoGenerics(), SwconfMappingException.class, "No generics found in field", objField, "! Select list element type in <> for map it!");
		
		var generics = mappingInfo.generics;
		
		ExceptionsUtils.If(generics.size() > 1, SwconfMappingException.class, "To many generics in field", objField, "! Field must has a single generic with list element type!");
		
		for (var entry : swconf.entries)
		{
			SwconfObject value = entry.value;
			
			if (value != null)
			{
				var typeGeneric = generics.first();
				
				var entryMappingInfo = MappingInfo.builder()
						.objType(typeGeneric.loadClass())
						.generics(typeGeneric.getChilds())
						.serialization(mappingInfo.getSerialization())
				.build();

				entryMappingInfo.setCustomAttributes(mappingInfo.getCustomAttributes());
				
				mappingInfo.getSerialization().deserialize(entryMappingInfo, value);
				
				list.add(entryMappingInfo.getObj());
			}
		}
		
		mappingInfo.setObj(list);
	}

	@Override
	public SwconfTable write(MappingInfo mappingInfo)
	{
		var list = (List) mappingInfo.getObj();
		
		if (list == null)
			return null;

		SwconfTable table = mappingInfo.getOrCreateInitialRoot(
				(initial) -> initial != null && initial.isTable() && !initial.asTable().hasKeys,
				() -> new SwconfTable().setHasKeys(false)
		);
		
		for (var element : list)
		{
			Class<?> elementType = null;

			if (element != null)
			{
				elementType = element.getClass();
			}
			else
			{
				elementType = mappingInfo.getGenerics().first().loadClass();
			}

			ExtendedList<GenericObject> elementGenerics = null;

			var firstGeneric = mappingInfo.getGenerics().first();
			if (firstGeneric != null)
				elementGenerics = CollectionsSWL.list(firstGeneric);
			else
				elementGenerics = new ExtendedList<>();

			var elementMappingInfo = MappingInfo.builder()
					.obj(element)
					.objType(elementType)
					.serialization(mappingInfo.getSerialization())
					.generics(elementGenerics)
			.build();

			elementMappingInfo.setCustomAttributes(mappingInfo.getCustomAttributes());
			
			var elementSwconf = mappingInfo.getSerialization().serialize(elementMappingInfo);
			
			if (elementSwconf != null)
			{
				table.put(elementSwconf);
			}
		}
		
		return table;
	}

	public <T extends ListMapper> T registerDefaultCreationFuns()
	{
	    registerCreationFun((info) -> {
			
			var type = info.getObjType();
			
			if (ExtendedList.class.isAssignableFrom(type))
				return CollectionsSWL.list();
			
			return null;
		});
		
		registerCreationFun((info) -> {
			
			var type = info.getObjType();
			
			if (List.class.isAssignableFrom(type))
				return new ArrayList<>();
			
			return null;
		});
		
		
		
		return (T) this;
	}
}
