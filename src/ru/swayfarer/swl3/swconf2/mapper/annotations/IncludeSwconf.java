package ru.swayfarer.swl3.swconf2.mapper.annotations;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;

@Inherited
@Retention(RUNTIME)
public @interface IncludeSwconf {
    public boolean include() default true;
}
