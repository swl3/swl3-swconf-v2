package ru.swayfarer.swl3.swconf2.mapper.annotations;

import lombok.var;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface EnableEnvs
{
    public String prefix();
}
