package ru.swayfarer.swl3.swconf2.mapper.annotations;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Inherited
@Retention(RUNTIME)
public @interface IgnoreSwconf
{

}
