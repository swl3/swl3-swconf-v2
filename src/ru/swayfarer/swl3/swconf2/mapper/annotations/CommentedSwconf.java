package ru.swayfarer.swl3.swconf2.mapper.annotations;

import lombok.var;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

@Retention(RUNTIME)
public @interface CommentedSwconf
{
	public String value() default "";
}
