package ru.swayfarer.swl3.swconf2.mapper;

import lombok.var;
import java.lang.reflect.Field;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.observable.event.AbstractEvent;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.reflection.generic.GenericObject;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.swconf2.types.SwconfObject;

@Getter
@Setter
@Accessors(chain = true)
@SuperBuilder
public class MappingInfo extends AbstractEvent {

    public static ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
    
	public SwconfSerialization serialization;
	public Object obj;
	public Object ownerInstance;
	public Class<?> objType;
	public Class<?> ownerClass;
	public Field objField;

	@Builder.Default
	public ExtendedList<GenericObject> generics = new ExtendedList<>();

	public SwconfObject initialRoot;

	public String fullName;
	
	public boolean hasNoGenerics()
	{
		return CollectionsSWL.isNullOrEmpty(generics);
	}

	public MappingInfo appendFullName(MappingInfo parent, String name)
	{
		return appendFullName(parent.getFullName(), name);
	}

	public MappingInfo appendFullName(String parent, String name)
	{
		if (!StringUtils.isBlank(parent))
			parent = parent + ".";

		fullName = StringUtils.orEmpty(parent) + StringUtils.orEmpty(name);
		return this;
	}

	public MappingInfoBuilder copy()
	{
		return MappingInfo.builder()
				.obj(this.getObj())
				.objField(this.getObjField())
				.objType(this.getObjType())
				.generics(this.getGenerics())
				.ownerClass(this.getOwnerClass())
				.ownerInstance(this.getOwnerInstance())
				.serialization(this.getSerialization())
		;
	}
	
	public static MappingInfoBuilder ofField(Field field, Class<?> ownerClass, Object ownerInstance, ReflectionsUtils reflectionsUtils)
	{
		return exceptionsHandler.safeReturn(() -> {
			return builder()
					.generics(reflectionsUtils.generics().of(field))
					.objField(field)
					.obj(field.get(ownerInstance))
					.objType(field.getType())
					.ownerClass(ownerClass)
					.ownerInstance(ownerInstance)
			;
		}, null, "Error while creating mapping info builder");
	}

	public <T extends SwconfObject> T getOrCreateInitialRoot(IFunction1<SwconfObject, Boolean> filter, IFunction0<T> valueFun)
	{
		if (initialRoot == null)
		{
			initialRoot = valueFun.apply();
		}
		else if (!Boolean.TRUE.equals(filter.apply(initialRoot)))
		{
			initialRoot = valueFun.apply();
		}

		return (T) initialRoot;
	}
}
