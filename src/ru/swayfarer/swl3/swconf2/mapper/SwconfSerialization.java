package ru.swayfarer.swl3.swconf2.mapper;

import lombok.var;
import java.io.File;
import java.util.UUID;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.chain.FunctionsChain1;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.swconf2.mapper.standart.ContstructorMapper;
import ru.swayfarer.swl3.swconf2.mapper.standart.EnumMapper;
import ru.swayfarer.swl3.swconf2.mapper.standart.ExpressionsListMapper;
import ru.swayfarer.swl3.swconf2.mapper.standart.ListMapper;
import ru.swayfarer.swl3.swconf2.mapper.standart.MapMapper;
import ru.swayfarer.swl3.swconf2.mapper.standart.NumberMapper;
import ru.swayfarer.swl3.swconf2.mapper.standart.PriorityListMapper;
import ru.swayfarer.swl3.swconf2.mapper.standart.ReflectionMapper;
import ru.swayfarer.swl3.swconf2.mapper.standart.StringValueOfMapper;
import ru.swayfarer.swl3.swconf2.types.SwconfBoolean;
import ru.swayfarer.swl3.swconf2.types.SwconfObject;
import ru.swayfarer.swl3.swconf2.types.SwconfString;

@Data
@Accessors(chain = true)
@SuppressWarnings({ "rawtypes", "unchecked"} )
public class SwconfSerialization {

	public ExtendedList<ISwconfMapper> registeredSwconfMappers = CollectionsSWL.list();

	public FunctionsChain1<String, String> commentFuns = new FunctionsChain1<String, String>()
			.setFilterFun((str) -> !StringUtils.isBlank(str))
	;

	public ISwconfMapper findMapperFor(@NonNull MappingInfo mappingInfo)
	{
		return registeredSwconfMappers.exStream()
				.first((e) -> e.isAccepts(mappingInfo));
	}
	
	public <T> T deserialize(Class<T> classOfObject, SwconfObject swconfObj)
	{
		var mappingInfo = prebuildMappingInfo(classOfObject)
				.initialRoot(swconfObj)
		.build();
		
		return deserialize(mappingInfo, swconfObj);
	}
	
	public <T> T deserialize(T obj, SwconfObject swconfObj)
	{
		var mappingInfo = prebuildMappingInfo(obj)
				.initialRoot(swconfObj)
		.build();
		
		return deserialize(mappingInfo, swconfObj);
	}

	public SwconfSerialization appendCommentsSource(IFunction1<String, String> commentsFun)
	{
		commentFuns.add(commentsFun);
		return this;
	}
	
	public <T extends SwconfObject> T serialize(Object obj)
	{
		var mappingInfo = prebuildMappingInfo(obj).build();
		return (T) serialize(mappingInfo);
	}
	
	public SwconfObject serialize(MappingInfo mappingInfo)
	{
		ISwconfMapper mapper = findMapperFor(mappingInfo);
		ExceptionsUtils.IfNull(mapper, SwconfMappingException.class, "No mapper found for mapping info", mappingInfo);
		return mapper.write(mappingInfo);
	}
	
	public <T> T deserialize(MappingInfo mappingInfo, SwconfObject swconfObj)
	{
		ISwconfMapper mapper = findMapperFor(mappingInfo);
		
		ExceptionsUtils.IfNull(mapper, SwconfMappingException.class, "No mapper found for mapping info", mappingInfo);
		
		mapper.read(swconfObj, mappingInfo);
		
		return (T) mappingInfo.getObj();
	}

	public MappingInfo.MappingInfoBuilder prebuildMappingInfo(Class<?> classOfInstance)
	{
		return MappingInfo.builder()
				.serialization(this)
				.objType(classOfInstance)
		;
	}

	public MappingInfo.MappingInfoBuilder prebuildMappingInfo(Object instance)
	{
		return MappingInfo.builder()
				.serialization(this)
				.obj(instance)
				.objType(instance.getClass())
		;
	}
	
	public <T extends SwconfSerialization> T registerConstructorMapper(Class<?> classOfObj, Class<? extends SwconfObject> classOfSwconf, Class... acceptableTypes)
	{
		return registerMapper(ContstructorMapper.builder()
				.classOfObj(classOfObj)
				.classOfSwconf(classOfSwconf)
				.acceptableClasses(CollectionsSWL.list(
						acceptableTypes
				)
			)
			.build()
		);
	}
	
	public <T extends SwconfSerialization> T registerMapper(ISwconfMapper<?, ?> mapper)
	{
		registeredSwconfMappers.add(mapper);
		return (T) this;
	}
	
	public static SwconfSerialization defaultMappers()
	{
	    var reflectionsUtils = ReflectionsUtils.getInstance();
		var ret = new SwconfSerialization();
		
		ret
            
		    .registerMapper(new ExpressionsListMapper())
            .registerMapper(new PriorityListMapper())
			
            .registerMapper(new ObservablePropertyMapper())
			
			.registerConstructorMapper(FileSWL.class, SwconfString.class, File.class)
			
			.registerMapper(new NumberMapper("byte", byte.class, Byte.class))
			.registerMapper(new NumberMapper("short", short.class, Short.class))
			.registerMapper(new NumberMapper("int", int.class, Integer.class))
			.registerMapper(new NumberMapper("long", long.class, Long.class))
			
			.registerMapper(new NumberMapper("float", float.class, Float.class))
			.registerMapper(new NumberMapper("double", double.class, Double.class))
			
			.registerConstructorMapper(Boolean.class, SwconfBoolean.class, boolean.class, Boolean.class)
			
			.registerConstructorMapper(String.class, SwconfString.class, String.class)

			.registerMapper(new ListMapper().registerDefaultCreationFuns())
			.registerMapper(new MapMapper().registerDefaultCreationFuns(reflectionsUtils))
			
			.registerMapper(new EnumMapper())

			.registerMapper(new StringValueOfMapper(UUID.class))
			
			.registerMapper(new ReflectionMapper()
			        .setReflectionsUtils(reflectionsUtils)
			        .registerDefaultEvents()
	        )
		;
		
		return ret;
	}
}
