package ru.swayfarer.swl3.swconf2.formats;

import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.io.path.PathHelper;

public abstract class AbtractSwconfExtensionFormatProvider implements IFormatProvider
{
    public ExtendedList<String> supportedExtensions = new ExtendedList<>();

    public boolean isAcceptingExtension(String ext)
    {
        return supportedExtensions == null ? false : supportedExtensions.contains(ext);
    }

    public boolean isAcceptingResource(String resourceName)
    {
        return isAcceptingExtension(PathHelper.getExtension(resourceName));
    }
}
