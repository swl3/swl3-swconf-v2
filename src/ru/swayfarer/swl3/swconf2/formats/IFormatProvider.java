package ru.swayfarer.swl3.swconf2.formats;

import ru.swayfarer.swl3.io.path.PathHelper;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.DataOutStream;
import ru.swayfarer.swl3.swconf2.types.SwconfTable;

public interface IFormatProvider {

	public SwconfTable readSwconf(DataInStream is);
	public void writeSwconf(SwconfTable swconfTable, DataOutStream os);
	public boolean isAcceptingResource(String resourceName);
	
}
