package ru.swayfarer.swl3.swconf2.formats;

import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.DataOutStream;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.swconf2.types.SwconfTable;

import java.io.InputStream;
import java.io.OutputStream;

public class InputStreamWrapperFormat implements IFormatProvider
{
    @Internal
    public IFormatProvider formatProvider;

    @Internal
    public IFunction1<InputStream, InputStream> inMapperFun;

    @Internal
    public IFunction1<OutputStream, OutputStream> outMapperFun;

    @Internal
    public ExtendedList<String> extensions = new ExtendedList<>();

    public InputStreamWrapperFormat(
            IFunction1<InputStream, InputStream> inMapperFun,
            IFunction1<OutputStream, OutputStream> outMapperFun,
            IFormatProvider formatProvider,
            String... exts
    )
    {
        this.formatProvider = formatProvider;
        this.inMapperFun = inMapperFun;
        this.outMapperFun = outMapperFun;
        this.extensions = CollectionsSWL.list(exts);
    }

    @Override
    public boolean isAcceptingResource(String resourceName)
    {
        for (var ext : extensions)
        {
            if (!ext.startsWith("."))
                ext = "." + ext;

            if (resourceName.endsWith(ext) && resourceName.length() > ext.length())
                resourceName = resourceName.substring(0, resourceName.length() - ext.length());

            var accepts = formatProvider.isAcceptingResource(resourceName);

            if (accepts)
                return true;
        }

        return false;
    }

    @Override
    public SwconfTable readSwconf(DataInStream is)
    {
        return formatProvider.readSwconf(is.to().wrap(inMapperFun));
    }

    @Override
    public void writeSwconf(SwconfTable swconfTable, DataOutStream os)
    {
        formatProvider.writeSwconf(swconfTable, os.to().wrap(outMapperFun));
    }
}
