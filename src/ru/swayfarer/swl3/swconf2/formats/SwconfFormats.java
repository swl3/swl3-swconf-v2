package ru.swayfarer.swl3.swconf2.formats;

import lombok.var;
import lombok.NonNull;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.io.path.PathHelper;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.DataOutStream;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.swconf2.types.SwconfTable;
import ru.swayfarer.swl3.swconf2.yaml.Yaml2SwconfProvider;

public class SwconfFormats {

	public static SwconfFormats INSTANCE = new SwconfFormats();

	public static ExtendedList<IFormatProvider> defaultFormatProviders = CollectionsSWL.list(
			Yaml2SwconfProvider.getInstance(),
			Yaml2SwconfProvider.getGzInstance(),
			Yaml2SwconfProvider.getXzInstance()
//			new Lua2SwconfProvider()
	);
	
	public ExtendedList<IFormatProvider> registeredFormatProviders = CollectionsSWL.list();
	
	public IFormatProvider findProviderForResource(@NonNull String resourceName)
	{
		IFormatProvider ret = registeredFormatProviders.exStream()
		        .findFirst((provider) -> provider.isAcceptingResource(resourceName))
		        .orNull()
        ;
		
		if (ret != null)
			return ret;
		
		ret = defaultFormatProviders.exStream()
		        .findFirst((provider) -> provider.isAcceptingResource(resourceName))
                .orNull()
        ;
		
		return ret;
	}
	
	public SwconfTable readResource(@NonNull DataInStream stream, @NonNull String resourceName)
	{
		var provider = findProviderForResource(resourceName);
		
		if (provider == null)
		{
			return null;
		}
		
		return stream == null ? null : provider.readSwconf(stream);
	}
	
	public void writeResource(@NonNull SwconfTable swconfTable, @NonNull DataOutStream os, @NonNull String resourceName)
	{
		var provider = findProviderForResource(resourceName);
		
		if (provider == null)
		{
			return;
		}
		
		provider.writeSwconf(swconfTable, os);
		os.close();
	}

	public static SwconfFormats getInstance()
	{
		return INSTANCE;
	}
}
