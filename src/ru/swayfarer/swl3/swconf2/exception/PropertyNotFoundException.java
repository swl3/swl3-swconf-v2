package ru.swayfarer.swl3.swconf2.exception;

@SuppressWarnings("serial")
public class PropertyNotFoundException extends RuntimeException {

    public PropertyNotFoundException()
    {
        super();
    }

    public PropertyNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public PropertyNotFoundException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public PropertyNotFoundException(String message)
    {
        super(message);
    }

    public PropertyNotFoundException(Throwable cause)
    {
        super(cause);
    }
}
