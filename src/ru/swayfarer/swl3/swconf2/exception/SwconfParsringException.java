package ru.swayfarer.swl3.swconf2.exception;

@SuppressWarnings("serial")
public class SwconfParsringException extends RuntimeException {

    public SwconfParsringException()
    {
        super();
    }

    public SwconfParsringException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public SwconfParsringException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public SwconfParsringException(String message)
    {
        super(message);
    }

    public SwconfParsringException(Throwable cause)
    {
        super(cause);
    }

}
