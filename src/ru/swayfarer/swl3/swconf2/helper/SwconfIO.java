package ru.swayfarer.swl3.swconf2.helper;

import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.exception.LibTags;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.io.link.RLUtils;
import ru.swayfarer.swl3.io.link.ResourceLink;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.DataOutStream;
import ru.swayfarer.swl3.swconf2.comment.CommentsFuns;
import ru.swayfarer.swl3.swconf2.formats.SwconfFormats;
import ru.swayfarer.swl3.swconf2.mapper.SwconfSerialization;
import ru.swayfarer.swl3.swconf2.types.SwconfTable;

import java.util.Properties;

public class SwconfIO {

	public static SwconfIO INSTANCE = new SwconfIO();
	
	public SwconfSerialization serialization = SwconfSerialization.defaultMappers();
	public SwconfFormats formats = SwconfFormats.getInstance();
	public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();

	public <T> T deserialize(@NonNull T instance, @NonNull ResourceLink rlink)
	{
		return deserialize(instance, rlink.in(), rlink.getPath());
	}
	
	public <T> T deserialize(@NonNull T instance, @NonNull DataInStream stream, @NonNull String resourceName)
	{
		return exceptionsHandler.handle()
			.tag(LibTags.Exceptions.tagSerialize)
			.message("Error while deserializing", instance, "from resource", resourceName)
			.andReturn(() -> {
				var swconf = formats.readResource(stream, resourceName);
				
				if (swconf != null)
				{
					return serialization.deserialize(instance, swconf);
				}
				
				return null;
			})
		;
	}
	
	public <T> T deserialize(@NonNull Class<T> classOfObj, @NonNull ResourceLink rlink)
	{
		return deserialize(classOfObj, rlink.in(), rlink.getPath());
	}
	
	public <T> T deserialize(@NonNull Class<T> classOfObj, DataInStream stream, @NonNull String resourceName)
	{
		if (stream == null)
			return null;
		
		return exceptionsHandler.handle()
			.tag(LibTags.Exceptions.tagSerialize)
			.message("Error while deserializing", classOfObj, "from resource", resourceName)
			.andReturn(() -> {
				var swconf = formats.readResource(stream, resourceName);
				
				if (swconf != null)
				{
					return serialization.deserialize(classOfObj, swconf);
				}
				
				return null;
			})
		;
	}
	
	public void serialize(@NonNull Object instance, @NonNull ResourceLink rlink)
	{
		serialize(instance, rlink.out(), rlink.getPath());
	}
	
	public void serialize(@NonNull Object instance, @NonNull DataOutStream dos, @NonNull String resourceName)
	{
		exceptionsHandler.handle()
			.tag(LibTags.Exceptions.tagSerialize)
			.message("Error while serializing", instance, "to resource", resourceName)
			.start(() -> {
				var swconf = serialization.serialize(instance);
				
				if (swconf != null && swconf instanceof SwconfTable)
				{
					formats.writeResource(swconf.asTable(), dos, resourceName);
				}
			})
		;
	}
    
    public SwconfIO useCommentsFrom(@NonNull Properties... props)
    {
        serialization.appendCommentsSource(CommentsFuns.ofProperties(props));
        return this;
    }
	
	public SwconfIO useCommentsFrom(@NonNull RLUtils rlUtils, @NonNull String... rlinks)
	{
	    serialization.appendCommentsSource(CommentsFuns.ofProperties(rlUtils, rlinks));
	    return this;
	}
	
	public static SwconfIO getInstance()
	{
		return INSTANCE;
	}
}
