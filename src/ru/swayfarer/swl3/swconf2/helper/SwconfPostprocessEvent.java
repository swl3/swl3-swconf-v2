package ru.swayfarer.swl3.swconf2.helper;

import lombok.var;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.observable.event.AbstractEvent;
import ru.swayfarer.swl3.swconf2.types.SwconfTable;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class SwconfPostprocessEvent extends AbstractEvent
{
    public String resourceName;
    public SwconfTable result;
}
