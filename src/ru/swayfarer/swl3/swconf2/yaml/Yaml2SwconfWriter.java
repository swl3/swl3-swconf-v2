package ru.swayfarer.swl3.swconf2.yaml;

import lombok.var;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.dynamic.DynamicString;
import ru.swayfarer.swl3.swconf2.types.SwconfTable;

public class Yaml2SwconfWriter {

	public String equalsSymbol = ": ";
	public String listSymbol = "- ";
	public String commentSymbol = "# ";
	public int indent = 2;

	public String write(SwconfTable table)
	{
		var buffer = new DynamicString();
		buffer.indentSpacesCount = indent;
		write(table, 0, buffer);
		buffer.newLine();
		return buffer.toString();
	}
	
	public void write(SwconfTable table, int currentIndent, DynamicString ret)
	{
	    int originalIndent = currentIndent;
		for (var entry : table.entries)
		{
            var value = entry.value;
            
            if (value.isTable() && value.asTable().entries.isEmpty())
                continue;
            
		    currentIndent = originalIndent;
			ret.indent(currentIndent);
			
			var comment = value.getComment();
			
			if (!StringUtils.isBlank(comment))
			{
				var lines = comment.split("\n");
				
				for (var line : lines)
				{
					ret.append(commentSymbol);
					ret.append(line);
					
					ret.newLine();
					ret.indent(currentIndent);
				}
			}
			
			if (table.hasKeys)
			{
				String key = entry.key;
				
				if (key.contains(": "))
					ExceptionsUtils.unsupportedOperation("Can't write key that contains ': ' to yaml syntax! It's system word. Please, don't use that key or try another format!");
				
				ret.append(key);
				ret.append(equalsSymbol);
			}
			else
			{
				ret.append(listSymbol);
			}
			
			if (value.isTable())
			{
				ret.newLine();
				write(value.asTable(), currentIndent + 1, ret);
			}
			else
			{
				String str = value.getValue().toString();
				
				if (value.isNum())
				{
				    str = StringUtils.toNumberString(value.asNum().getValue());
				}
				else
				{
				    str = value.getValue().toString();
				}
				
				if (value.isString())
				{
					if (str.contains("'"))
					{
						str = "\"" + str.replace("\"", "\\\"") + "\"";
					}
					else
					{
						str = "'" + str.replace("'", "\\'") + "'";
					}
				}
				
				ret.append(str);
				
				ret.append("\n");
			}
		}
		
		ret.newLine();
	}
	
}
