package ru.swayfarer.swl3.swconf2.yaml;

import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.DataOutStream;
import ru.swayfarer.swl3.io.xz.tukaani.XZInputStream;
import ru.swayfarer.swl3.io.xz.tukaani.XZOutputStream;
import ru.swayfarer.swl3.swconf2.formats.AbtractSwconfExtensionFormatProvider;
import ru.swayfarer.swl3.swconf2.formats.InputStreamWrapperFormat;
import ru.swayfarer.swl3.swconf2.types.SwconfTable;

import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class Yaml2SwconfProvider extends AbtractSwconfExtensionFormatProvider
{
	private static ExtendedList<String> extensions = CollectionsSWL.list("yaml", "yml");

	public static Yaml2SwconfProvider INSTANCE = new Yaml2SwconfProvider();

	public static InputStreamWrapperFormat GZ_INSTANCE = new InputStreamWrapperFormat(
			GZIPInputStream::new,
			GZIPOutputStream::new,
			INSTANCE,
			"gz"
	);

	public static InputStreamWrapperFormat XZ_INSTANCE = new InputStreamWrapperFormat(
			XZInputStream::new,
			XZOutputStream::new,
			INSTANCE,
			"xz"
	);

	@Override
	public boolean isAcceptingExtension(String ext)
	{
		return extensions.contains(ext);
	}

	@Override
	public SwconfTable readSwconf(DataInStream is)
	{
		return new Yaml2SwconfReader().readYaml(is);
	}

	@Override
	public void writeSwconf(SwconfTable swconfTable, DataOutStream os)
	{
		var str = new Yaml2SwconfWriter().write(swconfTable);
		os.writeString(str);
	}

	public static Yaml2SwconfProvider getInstance()
	{
		return INSTANCE;
	}

	public static InputStreamWrapperFormat getGzInstance()
	{
		return GZ_INSTANCE;
	}

	public static InputStreamWrapperFormat getXzInstance()
	{
		return XZ_INSTANCE;
	}
}
