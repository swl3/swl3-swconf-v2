package ru.swayfarer.swl3.swconf2.yaml;

import lombok.var;
import java.io.EOFException;
import java.io.InputStream;
import java.io.StringReader;

import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.io.streams.StreamsUtils;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.swconf2.exception.SwconfParsringException;
import ru.swayfarer.swl3.swconf2.types.SwconfObject;
import ru.swayfarer.swl3.swconf2.types.SwconfTable;
import ru.swayfarer.swl3.z.dependencies.org.ho.yaml.YamlDecoder;

public class Yaml2SwconfReader {

    public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
    
	public SwconfTable readYaml(InputStream yamlStream)
	{
	    String str = null;
	    
		try
		{
		    str = StreamsUtils.readAll("UTF-8", yamlStream).concat("\n");

		    if (StringUtils.isBlank(str))
			{
				return new SwconfTable();
			}

			var parser = new YamlDecoder(new StringReader(str));

			try
			{
				while (true)
				{
					var object = parser.readObject();
					SwconfObject swconfObject = SwconfObject.of(object);
					
					SwconfTable table = (SwconfTable) swconfObject;
					
					return table;
				}
			}
			catch (EOFException e)
			{
				
			}
		}
		catch (Throwable e)
		{
		    throw new SwconfParsringException("While parsing yaml text:\n*-*-*-*-*-*-*-*-*-Start-Of-File-*-*-*-*-*-*-*-*-*\n" + str + "\n*-*-*-*-*-*-*-*-*-End-Of-File-*-*-*-*-*-*-*-*-*", e);
		}
		
		return null;
	}

}
