package ru.swayfarer.swl3.z.dependencies.novoda.sexp;

import lombok.var;
import ru.swayfarer.swl3.z.dependencies.novoda.sax.RootElement;
import ru.swayfarer.swl3.z.dependencies.novoda.sexp.finder.ElementFinder;
import ru.swayfarer.swl3.z.dependencies.novoda.sexp.parser.ParseFinishWatcher;

public abstract class SimpleTagInstigator implements Instigator {

    private final ElementFinder<?> elementFinder;
    private final String elementTag;
    private final ParseFinishWatcher parseFinishWatcher;

    public SimpleTagInstigator(ElementFinder<?> elementFinder, String elementTag, ParseFinishWatcher parseFinishWatcher) {
        this.elementFinder = elementFinder;
        this.elementTag = elementTag;
        this.parseFinishWatcher = parseFinishWatcher;
    }

    @Override
    public void create(RootElement element) {
        elementFinder.find(element, elementTag);
    }

    @Override
    public void end() {
        parseFinishWatcher.onFinish();
    }
}
