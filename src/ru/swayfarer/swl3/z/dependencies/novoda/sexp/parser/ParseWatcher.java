package ru.swayfarer.swl3.z.dependencies.novoda.sexp.parser;

public interface ParseWatcher<T> {
    void onParsed(T item);
}
