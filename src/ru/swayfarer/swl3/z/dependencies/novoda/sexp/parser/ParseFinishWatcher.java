package ru.swayfarer.swl3.z.dependencies.novoda.sexp.parser;

public interface ParseFinishWatcher {
    void onFinish();
}
