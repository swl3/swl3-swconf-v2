package ru.swayfarer.swl3.z.dependencies.novoda.sexp.parser;

import lombok.var;
import ru.swayfarer.swl3.z.dependencies.novoda.sax.Element;

public interface Parser<T> {
    void parse(Element element, ParseWatcher<T> listener);
}

