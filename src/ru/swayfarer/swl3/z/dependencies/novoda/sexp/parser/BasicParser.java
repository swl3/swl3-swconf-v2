package ru.swayfarer.swl3.z.dependencies.novoda.sexp.parser;

import lombok.var;
import ru.swayfarer.swl3.z.dependencies.novoda.sax.Element;
import ru.swayfarer.swl3.z.dependencies.novoda.sax.EndTextElementListener;
import ru.swayfarer.swl3.z.dependencies.novoda.sexp.marshaller.BodyMarshaller;

public class BasicParser<T> implements Parser<T> {

    private final BodyMarshaller<T> bodyMarshaller;

    public BasicParser(BodyMarshaller<T> bodyMarshaller) {
        this.bodyMarshaller = bodyMarshaller;
    }

    @Override
    public void parse(Element element, final ParseWatcher<T> listener) {
        element.setEndTextElementListener(
                new EndTextElementListener() {
                    @Override
                    public void end(String body) {
                        listener.onParsed(bodyMarshaller.marshall(body));
                    }
                }
        );
    }

}
