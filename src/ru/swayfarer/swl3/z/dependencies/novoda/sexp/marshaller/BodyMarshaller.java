package ru.swayfarer.swl3.z.dependencies.novoda.sexp.marshaller;

public interface BodyMarshaller<T> {
    T marshall(String input);
}
