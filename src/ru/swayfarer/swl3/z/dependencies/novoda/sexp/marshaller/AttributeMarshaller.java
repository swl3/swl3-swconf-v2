package ru.swayfarer.swl3.z.dependencies.novoda.sexp.marshaller;

public interface AttributeMarshaller<T> {
    T marshall(String... input);
}
